<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class db_quires_model extends CI_Model
{
    /**
     * @param array $data
     * @param null $table
     * @return bool
     */
    public function insert($data = array(), $table = null){

        // check table and insert data
        if(!empty($data) && isset($table)){

            // insert data in table
            return $this->db->insert($table, $data);
        }else{
            return false;
        }
    }

    /**
     * @param array $data
     * @param array $cond
     * @param null $table
     */
    public function update($data = array(),$cond = array(), $table = null){
        if(isset($cond) && !empty($cond)){
            $this->db->where($cond);

            if(!empty($data) && isset($table)){
                return $this->db->update($table, $data);
            }
        }
    }

    /**
     * @param array $cond
     * @param null $table
     */
    public function delete($cond = array(), $table = null){

    }

    /**
     * @param array $cond
     * @param null $table
     */
    public function get($cond = array(), $table = null, $sql = null){
        if(!empty($cond)){
            $this->db->where($cond);
        }
        if($table){
            return $this->db->get($table);
        }
    }
}