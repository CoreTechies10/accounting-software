<?php

if(!function_exists('create_code')){
    function create_code($val1 = null, $val2 = null){
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFG0123456789!@#$%^&*(()_+-~`HIJKLMNOP%Q(R STUWXYZ";
        $rand ='';
        for ($i = 0; $i < 9; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $rand .= $alphabet[$n];
        }
        $code = hash('md5', $val1.$rand.$val2);
        return $code;
    }
}