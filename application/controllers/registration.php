<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class registration extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('core_model/db_quires_model');

    }

    public function index()
    {

        // form validation for new user login detail
        //$this->form_validation->set_rules('user_name', 'Username', 'required|is_unique[users.user_name]|max_length[15]'); // validation for user name

        $this->form_validation->set_rules('firstname', 'firstname', 'required|max_length[50]'); // validation for name
        $this->form_validation->set_rules('lastname', 'lastname', 'required|max_length[50]');// validation for name

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[50]|is_unique[user.email]');// validation for Email
        //$this->form_validation->set_rules('t_c', 'Term and Conditions', 'required');// validation for Term and conditions

        $this->form_validation->set_rules('password', 'Password', 'required|max_length[20]|min_length[8]');// validation for Password
        $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');// validation for Confirm Password
        $this->form_validation->set_rules('term_cond', 'Terms and Conditions', 'required');// validation for Confirm Password

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('registration/vwsignup');
        }
        else
        {
            // load helper for unique code
            $this->load->helper('create_code');

            //load library for UUID
            $this->load->library('UUID');


            $code = '';
            // user signup detail
            //hash('md5', $str);

            // first name and last name
            $post['firstname'] = $this->input->post('firstname');
            $post['lastname'] = $this->input->post('lastname');

            // email address
            $post['email'] = $this->input->post('email');

            // Universal Unique ID
            $post['user_id'] = $this->uuid->v5('A0EEBC99-9C0B-4EF8-BB6D-6BB9BD380A11', date('Y-m-d H:i:s'));

            // password
            $post['password'] = md5($this->input->post('c_password'));

            // Created Time
            $post['create_time'] = date('Y-m-d H:i:s');

            // Confirmation code
            $post['confirmation_code'] = $code = create_code($post['email'], $post['password']);

            // Client IP address
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $post['ip'] = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $post['ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $post['ip'] = $_SERVER['REMOTE_ADDR'];
            }

            // Inserted result
            $result = $this->db_quires_model->insert($post, 'user');

            if(isset($result) && $code){
                // Subject for confirmation mail
                $subject = 'Cal Kool Subscription Mail';

                $link = base_url().'registration/confirmation/'.urlencode($code);
                // message for confirmation mail
                $message = 'This is testing confirmation mail.<br>';

                $message .= 'Please <a href="'.$link.'">click Here for confirmation</a>.<br>';
                $message .= 'or simply go to the link: '. $link;

                // call mail function for confirmation link
                sendmail($post['email'], $message, $subject);

                echo 'confirmation mail is send to your mail id please confirm the mail and continue your registration process';
            }else{

                /*$data['alert'] = true;
                $this->load->view('header');
                $this->load->view('registration/registration', $data);
                $this->load->view('footer');*/
            }
        }
    }

    public function confirmation($code = null){
        if(isset($code) && $code != null){
            $result = $this->db_quires_model->get(array('confirmation_code'=> $code, 'confirmation_status'=> 0), 'user');
            if($result->num_rows>0){
                $result = $result->result_array();
                $update = $this->db_quires_model->update(array('confirmation_status'=> 1,'confirmation_code'=> '', 'email_verified_time' => date('Y-m-d H:i:s')), array('user_id'=> $result[0]['user_id']), 'user');
                if($update){
                    $data['message'] = 'You are successfully confirm with this protal<br> Please login and continue your registration process. ';
                    $data['message'] .= '<a href="'.base_url().'login">Login Here</a>';
                    $this->load->view('confirmation', $data);
                }
            }else{
                show_404();
            }

        }else{
            show_404();
        }

    }
}