<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller
{
    //
    public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('core_model/db_quires_model');
        $this->load->helper('create_code');


    }

    public function index(){
        //check for user login
        if($this->session->userdata('login_user') == true){
            redirect('home', 'refresh');
        }
        $data = array();
        // set validation rule
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() == FALSE){
            $this->load->view('login/vwlogin', $data);
        }else{
            $data = array(
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'confirmation_status' => 1,
                'forgot_status' => 0
            );
            $result = $this->db_quires_model->get($data, 'user');
            if($result->num_rows>0){
                $result = $result->result_array();
                $sess = array(
                    'user_id' => $result[0]['user_id'],
                    'email' => $result[0]['email'],
                );
                $this->session->set_userdata('login_user', $sess);
                redirect('home');

            }else{
                $data['error'] = 'Username and Password is Invalid';
                $this->load->view('login', $data);

            }

        }


    }

    public function logout(){
        //$this->login_model->logout();
        $this->session->sess_destroy();
        redirect('login');
    }

    public function forgot(){

        $data = array();
        $this->load->helper('captcha');

        if(isset($_POST['submit'])){

            // check form validation
            $this->form_validation->set_rules('email', 'Email', 'required');
            $val = $this->input->post('captcha');
            $this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_check_captcha['.$val.']');

            //
            if($this->form_validation->run() == true){
                $email = $this->input->post('email');

                $result = $this->db_quires_model->get(array('email'=>$email), 'user');
                if($result->num_rows > 0){

                    $result = $result->result_array();
                    // create custom code for reset password
                    $code = create_code($result[0]['email'],$result[0]['user_id']);
                    $link = base_url().'login/reset/'.$code;
                    $subject = 'Reset Password';
                    $message = 'This is testing mail for reset password<br>';
                    $message .= 'Please <a href="'.$link.'">Click on the link for reset password</a><br>';
                    $message .= 'or Directly got to this link: '.$link;

                    sendmail($email, $message, $subject);
                    $this->db_quires_model->update(array('confirmation_code'=> $code, 'forgot_status' => 1, 'forgot_pass_time' => date('Y-m-d H:i:s')), array('user_id'=> $result[0]['user_id']), 'user');
                    $this->session->set_flashdata('request_send', true);
                    redirect('login/reset');

                }else{
                    $data['error'] = 'This email Doesn`t exist';
                }
            }
        }


        $this->load->view('login/vwforgot', $data);

    }

    /**
     * @param null $code
     */
    public function reset($code = null){
        $data = array();

        //
        if($this->session->flashdata('request_send')){
            $data['data']['message'] = 'Your reset password mail is send to your email please confirm the link';
            $this->load->view('login/vwreset', $data);
        } else {
            if($code != null){
                $result = $this->db_quires_model->get(array('confirmation_code' => $code, 'confirmation_status' => 1, 'forgot_status' => 1),'user');

                // Check the confirmation code in database
                if($result->num_rows>0){
                    $result = $result->result_array();
                    if(isset($result[0]['forgot_pass_time']) && $result[0]['forgot_pass_time'] != ''){
                        $time = $result[0]['forgot_pass_time'];
                        $data['data']['email'] = $result[0]['email'];
                        $time = date('Y-m-d H:i:s', strtotime($time.' +6 minute'));

                        if(strtotime($time) > strtotime(date('Y-m-d H:i:s'))){
                            // check form submit
                            if(isset($_POST['submit'])){
                                $this->form_validation->set_rules('password', 'password', 'required');
                                $this->form_validation->set_rules('c_password', 'Confirm password', 'required|matches[password]');

                                if($this->form_validation->run() == true){

                                    $user_id = $result[0]['user_id'];
                                    $post['password'] = md5($this->input->post('password'));
                                    $post['confirmation_code'] = '';
                                    $post['forgot_status'] = 0;
                                    $update = $this->db_quires_model->update($post, array('user_id' => $user_id), 'user');
                                    if($update){
                                        $this->session->set_flashdata('reset_success', true);
                                        redirect('login');
                                    }

                                }

                            }
                            $data['data']['reset_pass'] = true;

                        }else{
                            $data['data']['message'] = 'Your Forgot session has been expired please <a href="'.base_url().'login/forgot">Click Here and Reset the Link</a> ';

                        }

                        $this->load->view('login/vwreset', $data);
                    }


                }else{
                    show_404();
                }
            }else{
                show_404();
            }
        }

    }

    public function check_captcha($val = null){
        if($val == null){
            $this->form_validation->set_message('check_captcha', 'Please enter correct words!');
            return false;
        }else {
            $str = $this->session->userdata('captcha');
            if(strcmp(strtoupper($str),strtoupper($val)) == 0){
                return true;
            }else{
                $this->form_validation->set_message('check_captcha', 'Please enter correct words!');
                return false;
            }
        }
    }
}