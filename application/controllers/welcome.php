<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('core_model/db_quires_model');
	}


	public function index()
	{

		//submit form for news letter
		if($this->input->post('submit_newsletter')){
			//server side validation for email
			$this->form_validation->set_rules('news_email', 'Email', 'required|valid_email|is_unique[ck_newsletter.news_email]|max_length[100]');

			// if validation run true
			if($this->form_validation->run() == true){
				$post = array(); // define post array

				//
				$post['news_email'] = $this->input->post('news_email');

				//get client`s ip
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					$post['news_ip'] = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$post['news_ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$post['news_ip'] = $_SERVER['REMOTE_ADDR'];
				}

				$post['news_timestampp'] = date('Y-m-d H:i:s');
				$result = $this->db_quires_model->insert($post, 'ck_newsletter');
				if($result){
					sendmail($post['news_email'] );
					$this->session->set_flashdata('news_letter', array('email' => $post['news_email']));
					redirect('welcome/thank_you', true);

				}
			}

		}

		//load default welcome view
		$this->load->view('welcome_message');
	}

	public function thank_you(){
		if($this->session->flashdata('news_letter')){
			$this->load->view('thank_you');
		}else{
			redirect('welcome');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */