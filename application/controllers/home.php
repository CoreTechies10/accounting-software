<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller
{
    //
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('core_model/db_quires_model');
        $this->load->helper('create_code');


    }

    public function index(){
        $user_data = $this->session->userdata('login_user');
        $data = array();
        if(isset($user_data['user_id'])){

            if(isset($_POST['submit'])){
                $this->form_validation->set_rules('firstname', 'firstname', 'required|max_length[50]'); // validation for name
                $this->form_validation->set_rules('lastname', 'lastname', 'required|max_length[50]');// validation for name
                $this->form_validation->set_rules('password', 'password', 'required|max_length[20]');// validation for name

                if($this->form_validation->run() == true){
                    $post['firstname'] = $this->input->post('firstname');
                    $post['lastname'] = $this->input->post('lastname');
                    $post['password'] = md5($this->input->post('password'));
                    $post['update_time'] = date('Y-m-d H:i:s');
                    $update = $this->db_quires_model->update($post, array('user_id'=> $user_data['user_id']), 'user');
                    if($update){
                        $data['update_success'] = 'Your Record Has been Updated';
                    }
                }

            }
            $result = $this->db_quires_model->get(array('user_id'=> $user_data['user_id']), 'user');
            if($result->num_rows>0){
                $data['user'] = $result->result_array()[0];
            }
            $this->load->view('home', $data);
        }else{
            show_404();
        }

    }
}