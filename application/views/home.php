<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Cal Kool - Sign Up</title>

    <style type="text/css">

        ::selection{ background-color: #E13300; color: white; }
        ::moz-selection{ background-color: #E13300; color: white; }
        ::webkit-selection{ background-color: #E13300; color: white; }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body{
            margin: 0 15px 0 15px;
        }

        p.footer{
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container{
            margin: 10px;
            border: 1px solid #D0D0D0;
            -webkit-box-shadow: 0 0 8px #D0D0D0;
        }
        .error{
            font-size: 10px;
            color: red;
        }
        .success{
            font-size: 10px;
            color: green;
        }
    </style>
</head>
<body>

<div id="container">
    <h1>Welcome to Cal Kool - Demo Home Page</h1>

    <div id="body">
        <p>This is for demo Home Page.</p>
        <a href="<?php echo base_url() ?>login/logout">Logout</a>
        <hr>
        <span class="success"><?php if(isset($update_success)) echo $update_success ?></span>
        <!--<p>If you would like to edit this page you'll find it located at:</p>
        <code>application/views/welcome_message.php</code>

        <p>The corresponding controller for this page is found at:</p>
        <code>application/controllers/welcome.php</code>
-->

        <table>
            <tr>
                <td>First Name :</td>
                <td><?php if(isset($user['firstname'])) echo $user['firstname'] ?></td>
            </tr>
            <tr>
                <td>Last Name :</td>
                <td><?php if(isset($user['lastname'])) echo $user['lastname'] ?></td>
            </tr>
            <tr>
                <td>Email :</td>
                <td><?php if(isset($user['email'])) echo $user['email'] ?></td>
            </tr>
        </table> <hr>
        <form method="post" >
            <table>
                <tr>
                    <td><label>First Name :</label></td>
                    <td>
                        <input type="text" placeholder="firstname" id="firstname" value="<?php echo set_value('firstname'); ?>" name="firstname" required="required" maxlength="50">
                        <br><span class="error"> <?php echo form_error('firstname'); ?></span>
                    </td>

                </tr>
                <tr>
                    <td><label>Last Name :</label></td>
                    <td>
                        <input type="text" placeholder="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>" name="lastname" required="required" maxlength="50">
                        <br><span class="error"> <?php echo form_error('lastname'); ?></span>
                    </td>
                </tr>
                <tr>
                    <td><label>Password :</label></td>
                    <td>
                        <input type="password" placeholder="Password" id="password" value="<?php echo set_value('password'); ?>" name="password" required="required" maxlength="50">
                        <br><span class="error"> <?php echo form_error('password'); ?></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="submit" value="Update"></td>
                </tr>
            </table>



        </form>
    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>
<script type="application/javascript">

    /*
     Custom validation for input fields
     over ride html5 formvalidation message
     */
    (function (exports) {
        function valOrFunction(val, ctx, args) {
            if (typeof val == "function") {
                return val.apply(ctx, args);
            } else {
                return val;
            }
        }

        function InvalidInputHelper(input, options) {
            input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

            function changeOrInput() {
                if (input.value == "") {
                    input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                } else {
                    input.setCustomValidity("");
                }
            }

            function invalid() {
                if (input.value == "") {
                    input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                } else {
                    input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
                }
            }

            input.addEventListener("change", changeOrInput);
            input.addEventListener("input", changeOrInput);
            input.addEventListener("invalid", invalid);
        }
        exports.InvalidInputHelper = InvalidInputHelper;
    })(window);



    InvalidInputHelper(document.getElementById("email"), {
        defaultText: "Please enter an email address!",
        emptyText: "Please enter an email address!",
        invalidText: function (input) {
            return 'The email address "' + input.value + '" is invalid!';
        }
    });
</script>
</body>
</html>