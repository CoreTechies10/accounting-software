<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Cal Kool</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	.error{
		font-size: 10px;
		color: red;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to Cal Kool - Demo News Letter Subscription Page</h1>

	<div id="body">
		<p>This is for demo News letter Page.</p>
		<a href="<?php echo base_url() ?>login/">Login</a>
		<a href="<?php echo base_url() ?>registration/">Sign Up</a>
		<hr>

		<!--<p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/welcome.php</code>
-->
		<form method="post">
			<label>Enter Your Email Address :</label>
			<input type="email" placeholder="Enter Your Email Address" id="email" value="<?php echo set_value('news_email'); ?>" name="news_email" required="required" maxlength="100">
			<input type="submit" name="submit_newsletter" value="Subscribe Now"><br>
			<span class="error"> <?php echo form_error('news_email'); ?></span>
		</form>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>
<script type="application/javascript">

	/*
	Custom validation for input fields
	over ride html5 formvalidation message
	 */
	(function (exports) {
		function valOrFunction(val, ctx, args) {
			if (typeof val == "function") {
				return val.apply(ctx, args);
			} else {
				return val;
			}
		}

		function InvalidInputHelper(input, options) {
			input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

			function changeOrInput() {
				if (input.value == "") {
					input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
				} else {
					input.setCustomValidity("");
				}
			}

			function invalid() {
				if (input.value == "") {
					input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
				} else {
					input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
				}
			}

			input.addEventListener("change", changeOrInput);
			input.addEventListener("input", changeOrInput);
			input.addEventListener("invalid", invalid);
		}
		exports.InvalidInputHelper = InvalidInputHelper;
	})(window);



	InvalidInputHelper(document.getElementById("email"), {
		defaultText: "Please enter an email address!",
		emptyText: "Please enter an email address!",
		invalidText: function (input) {
			return 'The email address "' + input.value + '" is invalid!';
		}
	});
</script>
</body>
</html>