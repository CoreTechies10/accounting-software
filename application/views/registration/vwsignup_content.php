
<section>
    <div class="container-fluid" id="section_login">
        <ul class="cb-slideshow hidden-xs" >
            <li><span>Image 01</span></li>
            <li><span>Image 02</span><div><h3></h3></div></li>
            <li><span>Image 03</span><div><h3></h3></div></li>
            <li><span>Image 04</span><div><h3></h3></div></li>
            <li><span>Image 05</span><div><h3></h3></div></li>
            <li><span>Image 06</span><div><h3></h3></div></li>
        </ul>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 centered div_login">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 div_login_1">
                        <h4>Keep yourself accounted with <span class="calkool-tm">Calkool</span>&trade;</h4>
                        <ul>
                            <li><img src="<?php echo base_url() ?>assets/img/1.jpg" alt="." width="172" height="172"></li>
                            <li>
                                <p>
                                    Sign Up online anytime, anywhere on your Mac, PC, tablet or phone and see up-to-date financial. It`s small business accounting software that`s simple, smart and occasionally magical.
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 div_login_2">
                        <form method="post" novalidate>
                            <h3>Sign Up</h3>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="First Name" name="firstname" value="<?php echo set_value('firstname'); ?>" required="required" maxlength="50">
                                    <span class="error"> <?php echo form_error('firstname'); ?></span>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="<?php echo set_value('lastname'); ?>" required="required" maxlength="50">
                                    <span class="error"> <?php echo form_error('lastname'); ?></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <input type="email" class="form-control" placeholder="Email Address" name="email" value="<?php echo set_value('email'); ?>" required="required" maxlength="50">
                                    <span class="error"> <?php echo form_error('email'); ?></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <input type="password" class="form-control" placeholder="Password" id="password" value="<?php //echo set_value('password'); ?>" name="password" required="required" maxlength="50">
                                    <span class="error"> <?php echo form_error('password'); ?></span>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <input type="password" class="form-control" placeholder="Confirm Password" id="c_password" value="<?php //echo set_value('c_password'); ?>" name="c_password" required="required" maxlength="50">
                                    <span class="error"> <?php echo form_error('c_password'); ?></span>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <label>
                                        <input type="checkbox" name="term_cond" style="vertical-align:bottom;">
                                        I accept the privacy policy and terms of service <a href="#teamPopup" data-toggle="modal">(show)</a>

                                    </label>
                                    <span class="error"> <?php echo form_error('term_cond'); ?></span>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-simple">Sign Up</button>
                            <a class="btn btn-simple" href="<?php echo base_url() ?>login">Back</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!--<div class="row div_foot">
                    <a href='#'>Don't have an account?</a>
                </div>-->
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="teamPopup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="teamPopup-title">Terms and Conditions</h4>
            </div>
            <div class="modal-body" style="padding:0 15px">




            </div>
        </div>
    </div>
</div>
