<?php
//random characters
$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
$rand ='';
for ($i = 0; $i < 4; $i++) {
    $n = rand(0, strlen($alphabet)-1);
    $rand .= $alphabet[$n];
}

// create captcha variable
$captcha = array(
    'word' => $rand,
    'img_url' => base_url().'captcha/',
    'img_path' => './captcha/',
    'font_path' => base_url().'system/fonts/texb.ttf',
    'img_width' => '150',
    'img_height' => '30',
    'expiration' => 7200,
);

// create captcha
$data['cap']= create_captcha($captcha);

if(isset($error)){
    $data['error'] = $error;

}
// store captcha in session
$this->session->set_userdata('captcha', $data['cap']['word']);

// load views
$this->load->view('vwheader');
$this->load->view('login/vwforgot_content', $data);
$this->load->view('vwfooter');