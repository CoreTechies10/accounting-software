
<section>
    <div class="container-fluid" id="section_login">
        <ul class="cb-slideshow hidden-xs" >
            <li><span>Image 01</span></li>
            <li><span>Image 02</span><div><h3></h3></div></li>
            <li><span>Image 03</span><div><h3></h3></div></li>
            <li><span>Image 04</span><div><h3></h3></div></li>
            <li><span>Image 05</span><div><h3></h3></div></li>
            <li><span>Image 06</span><div><h3></h3></div></li>
        </ul>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 centered div_login">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-12 div_login_1">
                        <h4>Keep yourself accounted with <span class="calkool-tm">Calkool</span>&trade;</h4>
                        <ul>
                            <li><img src="<?php echo base_url() ?>assets/img/officekit.jpg" alt="." width="172" height="172"></li>
                            <li>
                                <p>
                                    Log in online anytime, anywhere on your Mac, PC, tablet or phone and see up-to-date financial. It`s small business accounting software that`s simple, smart and occasionally magical.
                                </p>
                            </li>
                        </ul>
                    </div>

                    <?php if(isset($message)):  ?>
                        <div class="col-lg-5 col-md-5 col-sm-12 div_login_2" style="text-align: center">
                            <h3>Reset Password</h3>
                        <span> <?php echo $message; ?>
                                </span>
                        </div>

                    <?php endif; ?>
                    <?php
                    if(isset($reset_pass)):
                    ?>
                    <form method="post" novalidate>
                        <div class="col-lg-5 col-md-5 col-sm-12 div_login_2">
                            <h3>Reset Password</h3>
                            <div class="form-group">
                                <label> Email : <?php if(isset($email)) echo $email ?></label>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" required="required" maxlength="50" placeholder="Password">
                                <span class="error"> <?php echo form_error('password'); ?></span>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="c_password" required="required" maxlength="50" placeholder="Confirm Password">
                                <span class="error"> <?php echo form_error('c_password'); ?></span>
                            </div>
                            <button type="submit" name="submit" class="btn btn-simple">Reset Password</button>
                            <br/><br/><br/><br/>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <?php endif; ?>
                </div>
                <div class="row div_foot">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

