<section>
    <div class="container-fluid" id="section_login">
        <ul class="cb-slideshow hidden-xs" >
            <li><span>Image 01</span></li>
            <li><span>Image 02</span><div><h3></h3></div></li>
            <li><span>Image 03</span><div><h3></h3></div></li>
            <li><span>Image 04</span><div><h3></h3></div></li>
            <li><span>Image 05</span><div><h3></h3></div></li>
            <li><span>Image 06</span><div><h3></h3></div></li>
        </ul>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-8 centered div_login">
                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 div_login_2">
                        <form method="post" novalidate>
                            <h3>Foregot Password</h3>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Enter Your Email Address" id="email" value="<?php echo set_value('email'); ?>" name="email" required="required" maxlength="50">
                                <span class="error"> <?php echo form_error('email'); ?></span>
                                <span class="error"><?php if(isset($error)) echo $error; ?></span>
                            </div>
                            <div class="form-group">
                                <?php echo $cap['image'] ?>
                            </div>
                            <div class="form-group">

                                <input type="text" class="form-control" name="captcha" required="required" maxlength="15" placeholder="Please Enter Captcha" required>
                                <span class="error"> <?php echo form_error('captcha'); ?></span>
                            </div>
                            <button type="submit" name="submit" class="btn btn-simple">Send Mail</button>
                            <a href="<?php echo base_url() ?>login" class="btn btn-simple">Back</a>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

