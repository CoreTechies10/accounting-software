--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-01-28 18:51:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2136 (class 1262 OID 16393)
-- Dependencies: 2135
-- Name: calkool; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE calkool IS 'calkool database for accounting software';


--
-- TOC entry 187 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 187
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 16410)
-- Name: ck_newsletter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ck_newsletter (
    news_id integer NOT NULL,
    news_email character varying(100),
    news_ip character varying(25),
    news_timestampp character varying(25)
);


ALTER TABLE ck_newsletter OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 16408)
-- Name: ck_newsletter_news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ck_newsletter_news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ck_newsletter_news_id_seq OWNER TO postgres;

--
-- TOC entry 2140 (class 0 OID 0)
-- Dependencies: 180
-- Name: ck_newsletter_news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ck_newsletter_news_id_seq OWNED BY ck_newsletter.news_id;


--
-- TOC entry 183 (class 1259 OID 16479)
-- Name: ck_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ck_user (
    firstname character varying(50),
    lastname character varying(50),
    email character varying(100),
    email_verified_time character varying(20),
    last_login timestamp without time zone,
    create_time timestamp without time zone,
    update_time timestamp without time zone,
    ip character varying(20),
    user_id integer NOT NULL,
    confirmation_code character varying(50),
    confirmation_status integer DEFAULT 0,
    password character varying(50)
);


ALTER TABLE ck_user OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16512)
-- Name: ck_user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ck_user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ck_user_user_id_seq OWNER TO postgres;

--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 184
-- Name: ck_user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ck_user_user_id_seq OWNED BY ck_user.user_id;


--
-- TOC entry 185 (class 1259 OID 24588)
-- Name: newsletter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE newsletter (
    news_id uuid NOT NULL,
    news_email character varying(100),
    news_timestampp character varying(25),
    news_ip character varying(25)
);


ALTER TABLE newsletter OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16434)
-- Name: sno_users; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sno_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sno_users OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 24597)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    user_id uuid NOT NULL,
    email character varying(100),
    create_time timestamp without time zone,
    update_time timestamp without time zone,
    email_verified_time timestamp without time zone,
    last_login timestamp without time zone,
    ip character varying(20),
    confirmation_code character varying(50),
    forgot_pass_time timestamp without time zone,
    confirmation_status integer DEFAULT 0,
    password character varying(50),
    firstname character varying(50),
    lastname character varying(50),
    forgot_status integer DEFAULT 0 NOT NULL
);


ALTER TABLE "user" OWNER TO postgres;

--
-- TOC entry 1997 (class 2604 OID 16413)
-- Name: news_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ck_newsletter ALTER COLUMN news_id SET DEFAULT nextval('ck_newsletter_news_id_seq'::regclass);


--
-- TOC entry 1998 (class 2604 OID 16514)
-- Name: user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ck_user ALTER COLUMN user_id SET DEFAULT nextval('ck_user_user_id_seq'::regclass);


--
-- TOC entry 2125 (class 0 OID 16410)
-- Dependencies: 181
-- Data for Name: ck_newsletter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ck_newsletter (news_id, news_email, news_ip, news_timestampp) FROM stdin;
17	hgvhgvhgv	fhghygh	hghg
\.


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 180
-- Name: ck_newsletter_news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ck_newsletter_news_id_seq', 17, true);


--
-- TOC entry 2127 (class 0 OID 16479)
-- Dependencies: 183
-- Data for Name: ck_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ck_user (firstname, lastname, email, email_verified_time, last_login, create_time, update_time, ip, user_id, confirmation_code, confirmation_status, password) FROM stdin;
Himanshu 123	Joshi	himsjoshi27@gmail.com	\N	\N	2016-01-23 11:41:57	2016-01-25 10:52:16	\N	5		1	5f4dcc3b5aa765d61d8327deb882cf99
Himanshu 	superadmin	himanshu@coretechies.org	2016-01-25 10:57:44	\N	2016-01-25 10:56:46	2016-01-25 10:58:25	::1	6		1	5f4dcc3b5aa765d61d8327deb882cf99
\.


--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 184
-- Name: ck_user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ck_user_user_id_seq', 6, true);


--
-- TOC entry 2129 (class 0 OID 24588)
-- Dependencies: 185
-- Data for Name: newsletter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY newsletter (news_id, news_email, news_timestampp, news_ip) FROM stdin;
\.


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 182
-- Name: sno_users; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sno_users', 1, false);


--
-- TOC entry 2130 (class 0 OID 24597)
-- Dependencies: 186
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "user" (user_id, email, create_time, update_time, email_verified_time, last_login, ip, confirmation_code, forgot_pass_time, confirmation_status, password, firstname, lastname, forgot_status) FROM stdin;
ca5e7a3f-59a2-52d7-8f5b-4009db8ac0a4	himanshu@coretechies.org	2016-01-28 12:18:36	\N	2016-01-28 12:22:56	\N	::1	3a78338ce971fce01c5f158a9e131df0	2016-01-28 14:19:22	1	5f4dcc3b5aa765d61d8327deb882cf99	Himanshu 	Joshi	1
\.


--
-- TOC entry 2005 (class 2606 OID 16519)
-- Name: PK; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ck_user
    ADD CONSTRAINT "PK" PRIMARY KEY (user_id);


--
-- TOC entry 2003 (class 2606 OID 16415)
-- Name: ck_newsletter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ck_newsletter
    ADD CONSTRAINT ck_newsletter_pkey PRIMARY KEY (news_id);


--
-- TOC entry 2007 (class 2606 OID 24592)
-- Name: pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY newsletter
    ADD CONSTRAINT pk PRIMARY KEY (news_id);


--
-- TOC entry 2009 (class 2606 OID 24602)
-- Name: user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pk PRIMARY KEY (user_id);


--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-01-28 18:51:19

--
-- PostgreSQL database dump complete
--

